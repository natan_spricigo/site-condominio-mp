/*!
 * Start Bootstrap - Freelancer Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('.page-scroll a').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Floating label headings for the contact form
$(function() {
    $("body").on("input propertychange", ".floating-label-form-group", function(e) {
        $(this).toggleClass("floating-label-form-group-with-value", !! $(e.target).val());
    }).on("focus", ".floating-label-form-group", function() {
        $(this).addClass("floating-label-form-group-with-focus");
    }).on("blur", ".floating-label-form-group", function() {
        $(this).removeClass("floating-label-form-group-with-focus");
    });
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
})

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});

var urls = [["agenda","./pages/agenda.html"],["depoimentos","./pages/comments.html"],["imgs","./pages/fotos.html"]];

$.map(urls,function(n,i){
    $.ajax({
        url: n[1],
        beforeSend:function(){
            $("#"+n[0]).html("<div class='col-xs-12 text-center'><i class='fa fa-circle-o-notch fa-5x fa-spin'></i></div>");
        },
        success:function(data){
            $("#"+n[0]).html(data);
        },
        error:function(){
            $("#"+n[0]).html("<div class='col-xs-12 text-center'><i class='fa fa-times fa-5x fa-spin'></i></div>");  
        }
    });
});


$.ajax({
    url: "pages/login.html",
    success:function(data){
        $('#login').popover({
            html : true,
            title : "Login",
            placement : "bottom",
            content : data
        });
    }
});
