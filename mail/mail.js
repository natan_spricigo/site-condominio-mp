//agr12grt345
var send = true;
$(document).on('submit','form#contact',function(e){
  e.preventDefault();
  if(send){
    //Messages Calls => https://mandrillapp.com/api/docs/messages.html        
    $.ajax({
      type: 'POST',
      url: 'https://mandrillapp.com/api/1.0/messages/send.json',
      data: em("form#contact"),
      success: function(response) {
       $('#success').text('O email foi enviado, obrigado pelo contato!');
       $('form#contact')[0].reset();
     },
     error:function(e) {
       $('#success').text('Ops... houve um erro: '+e.status+' '+e.statusText+'.');
     },
     beforeSend:function(){
     	$('#success').text('Enviando email...');
     }
     });
  }
});

function em(form){
	var $this = $(form);
	var ob = {};
	ob.nome = $this.find("[name=nome]").val();
	ob.email = $this.find("[name=email]").val();
	ob.telefone = $this.find("[name=telefone]").val();
	ob.ms = $this.find("[name=mensagem]").val().replace("/\n/g","<br/>");

	return {
        'key': 'm1WBt5WY1IP7xbN2kMKByA',
        'message': {
          'from_email': decodeURIComponent(ob.email),
          'to': [
              {
                'email':'natan.spricigo@gmail.com',
                'name': 'Natan',
                'type': 'to'
              },
              {
                'email':ob.email,
                'name': ob.nome,
                'type': 'to'
              },
            ],
          'autotext': 'true',
          'subject': 'Contato com o condominio, nome: '+ob.nome,
          'html':gerarHtmlEmail(ob)
        }
      }
}
function gerarHtmlEmail(j){
	return "<div>"+
		"Nome : "+j.nome+"<br>"+
		"Email: "+j.email+"<br>"+
		"Telefone: "+j.telefone+"<br>"+
		"Mensagem: <br/>"+
		"<p>"+j.ms+"</p>"+
	"<div>";
}